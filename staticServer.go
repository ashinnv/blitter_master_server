package main

import (
	"bufio"
	"bytes"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"os/exec"
	"strings"
)

func main() {

	//go receivePost()
	//go sendPage()
	go handleIndx()

	input := bufio.NewScanner(os.Stdin)
	input.Scan()

}

//get a req that contains post data
func receivePost() {}

func handleIndx() {
	snd := getSendPage()
	http.HandleFunc("/", snd)
	http.ListenAndServe(":80", nil)
}

//send off a page, including a homepage
func getSendPage() string {

	baseTemplate := genBaseTempl()
	return baseTemplate
}

func errHandl(input error) {
	fmt.Println("Error:", input)
}

//Return a slice of strings associated with the user's expected wants and stuff.
func getForWho(forWho string) []string {
	cmd := exec.Command("../haskellStandIn/standIn")
	var out bytes.Buffer
	cmd.Stdout = &out

	err := cmd.Run()

	if err != nil {
		fmt.Println("ERROR STUFF:", err)
	}

	forWho = string(out.String())
	ret := strings.Split(forWho, ",")

	return ret
}

//Generate the index.html to be served up.
func genBaseTempl(input chan string, output chan string) {

	for forWho := range input {
		pageStringData, err := ioutil.ReadFile("../htmlTemplates/main.tTmp")
		if err != nil {
			errHandl(err)
		}

		associations := getForWho(forWho)
		postRefs := getPostRefs(associations)
		sendoff := buildFinal(postRefs)

		output <- sendoff
	}

}
