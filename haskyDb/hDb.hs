import Control.Monad
import Data.Char

main = do--forever $ do
    let fileText = getDat
    
    let res = keyToVal "Tom7my" 0
    print(res)

--returns first instance of tuple where first string matches the 'key' 
keyToVal::String->Int->(String,String,String)
keyToVal key iteration
    | getTupFirst(tupleKeyVal!!iteration) == key = tupleKeyVal!!iteration
    | otherwise = do
        keyToVal key (iteration+1)

getTupFirst::(String,String,String)->String
getTupFirst (x,y,z) = x    

--maintain a string from the text file's contents
getDat::String->IO String
getDat = do
    let s = readFile "test.txt"
    return s



--Test data
tupleKeyVal = [("Jeffery","Cats and Shit","I don't care about cats."),("Tom7my", "7The Meaning of Life", "7Fuck bitches, get paid."),("Jeffery","Cats and Shit","I don't care about cats."),("Elga", "Who moved my cheese", "Some fucking mouse.")]